#include <Arduino.h>

#define TMS_PIN 8
#define TCK_PIN 9
#define TDI_PIN 10
#define TDO_PIN 11

#define BYPASS 0x1F
#define EXTEST 0x06
#define SAMPLE 0x02
#define HIGHZ 0x00

char incomingByte = 0;
uint32_t id = 0;
unsigned char buffer[19];
unsigned char idcode[4];
unsigned char tdi[19];
int led_state = 0;

void TMS(int value)
{
    digitalWrite(TMS_PIN, value);
}

void TCK()
{
    digitalWrite(TCK_PIN, HIGH);
    delayMicroseconds(20);
    digitalWrite(TCK_PIN, LOW);
    delayMicroseconds(20);
}

void TCK(int clocks)
{
    for (int i = 0; i < clocks; i++)
        TCK();
}

// Writes to PIC32 TDI
void TDO(int value)
{
    digitalWrite(TDO_PIN, value);
}

void setTDO(unsigned char *value, char const *string)
{
    unsigned char aux[19];
    int bit;
    for(int i = 0; i < 19 ; i++){
        aux[i] = value[i];
    }

    int count = 0;
    if (strcmp(string, "IR") == 0)
    {
        for (int i = 0; i < 4; i++)
        {
            TDO(*aux & 0x01);
            TCK();
            *aux >>= 1;
        }
        TDO(*aux & 0x01);
    }

    else if (strcmp(string, "DR") == 0)
    {
        for (int i = 0; i < 19; i++)
        {
            tdi[i] = 0;
            for (int j = 0; j < 8; j++)
            {
                if (count == 147)
                    break;

                bit = digitalRead(TDI_PIN);
                tdi[i] = tdi[i] | (bit << j);
                TDO((aux[i] & 0x80) >> 7);
                TCK();
                aux[i] <<= 1;
                count++;
            }
        }
        TDO(*aux & 0x01);
    }
}

int readChar()
{
    while (Serial.available() == 0)
        ;
    // read the incoming byte:
    return Serial.read();
}

//Goes to Run-Test/Idle
void resetTAP()
{
    TMS(1);
    for (int i = 0; i < 5; i++)
    {
        TCK();
    }
    TMS(0);
    TCK();
}

void setSM(int value, int shifts)
{
    int aux = value;

    for (int i = 0; i < shifts; i++)
    {
        TMS(aux & 0x01);
        TCK();
        aux >>= 1;
    }
}

void led(boolean on)
{
    unsigned char tdo_ir[1];
    int sm[3];
    tdo_ir[0] = EXTEST;
    sm[0] = 0x03;
    sm[1] = 0x0b;
    sm[2] = 0x03;
    for (int i = 0; i < 19; i++)
    {
        buffer[i] = 0;
    }

    if (on){
        buffer[2] = 0x18;
        led_state = 1;
    }
    else
        led_state = 0;


    setSM(sm[0], 4); // Goes from Run-Test/Idle to Shift-IR
    setTDO(tdo_ir, "IR");
    setSM(sm[1], 6); // Goes from Shift-IR to Shift-DR
    setTDO(buffer, "DR");

    setSM(sm[2], 3); //Goes from Shift-DR to Run-Test/Idle

}

void getID()
{
    unsigned char tdo[1];
    unsigned char *aux = buffer;
    int sm[3];
    tdo[0] = 0x01;
    sm[0] = 0x03;
    sm[1] = 0x0b;
    sm[2] = 0x03;
    int bit = 5;
    setSM(sm[0], 4); // Goes from Run-Test/Idle to Shift-IR
    setTDO(tdo, "IR");
    setSM(sm[1], 6); // Goes from Shift-IR to Shift-DR


    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            bit = digitalRead(TDI_PIN);
            idcode[3 - i] = idcode[3 - i] | (bit << j);
            TCK();
        }
    }
    TDO(*aux & 0x01);

    setSM(sm[2], 3); //Goes from Shift-DR to Run-Test/Idle

    if(led_state)
        led(true);
}

int buttonState()
{
    unsigned char tdo_ir[1];
    int sm[3];
    tdo_ir[0] = SAMPLE;
    sm[0] = 0x03;
    sm[1] = 0x0b;
    sm[2] = 0x03;

    for (int i = 0; i < 19; i++)
    {
        buffer[i] = 0;
    }


    setSM(sm[0], 4); // Goes from Run-Test/Idle to Shift-IR
    setTDO(tdo_ir, "IR");
    setSM(sm[1], 6); // Goes from Shift-IR to Shift-DR
    setTDO(buffer, "DR");

    setSM(sm[2], 3); //Goes from Shift-DR to Run-Test/Idle

     if(led_state)
        led(true);

    return (tdi[0]>>3) & 0x01;
}

void setup()
{
    pinMode(TMS_PIN, OUTPUT);
    pinMode(TCK_PIN, OUTPUT);
    pinMode(TDO_PIN, OUTPUT);
    pinMode(TDI_PIN, INPUT);

    pinMode(13, OUTPUT);

    digitalWrite(TCK_PIN, LOW);
    digitalWrite(TMS_PIN, LOW);
    Serial.begin(9600);
    Serial.println("d - IDCODE");
    Serial.println("b - btn state");
    Serial.println("1 - Turn On LED5");
    Serial.println("0 - Turn OFF LED5");
    resetTAP();
}

void loop()
{
    incomingByte = readChar();
    switch (incomingByte)
    {
    case '0':
        resetTAP();
        led(false);
        break;
    case '1':
        resetTAP();
        led(true);
        break;
    case 'd':
        resetTAP();
        getID();
        Serial.print("IDCODE: 0x");
        for (int i = 0; i < 4; i++)
        {
            Serial.print(idcode[i], HEX);
        }
        Serial.println();

        break;
    case 'b':
        Serial.println("Button State: ");
        Serial.println(!buttonState());
        break;
    default:
        break;
    }
}