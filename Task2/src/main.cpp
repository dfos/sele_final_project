#include <Arduino.h>
#include <i2c.h>

unsigned char received_byte = 0;
uint16_t luminosity;


uint8_t power_on()
{
  if (!start())
    return 0;
  if (!sendByte((0x39 << 1)))
    return 0;
  if (!sendByte(0x80))
    return 0;
  if (!sendByte(0x03))
    return 0;

  stop();

  return 1;
}

uint16_t getData(uint16_t *rx)
{
  uint8_t x,t;
  uint16_t luminosity;

  //Sends START
  if (!start())
    return 0;
  //Sends ADDRESS + WRITE
  if (!sendByte((0x39 << 1)))
    return 0;
  //Writes to channel 0 register of the sensor
  if (!sendByte(0x80 | 0x20 | 0x0C))
    return 0;
  stop();

  //Sends START
  if (!start())
    return 0;
  //Sends ADDRESS + READ
  if (!sendByte((0x039 << 1) + 0x01))
    return 0;
  //Reads first byte from channel 0
  if (!receiveByte(0, &x))
    return 0;
  //Reads sencond byte from channel 0
  if (!receiveByte(1, &t))
    return 1;
 
  stop();

  luminosity = x << 8;
  luminosity |= t;

  *rx = luminosity;

  return 1;
}
void setup()
{
  Serial.begin(9600);
  initialize();

  if (!power_on())
    Serial.println("Could not power on sensor");

  Serial.println("");

}


void loop()
{

  if (!getData(&luminosity))
    Serial.println("Could not get luminosity from sensor");
  Serial.print("luminosity: ");
  Serial.println(luminosity);
  delay(500);
}