#include "i2c.h"

void initialize()
{
  TWSR = 0;
  TWBR = ((F_CPU / I2C_CLK) - 16) / 2;
  TWCR = (1 << TWEN);
}

uint8_t start(void)
{
  //Wait for TWINT flag set. This indicates that the START condition has been transmitted
  TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
  while (!(TWCR & (1 << TWINT)))
    ;

  //Check value of TWI status register.
  if (TWI_STATUS != 0x08 && TWI_STATUS != 0x10)
    return 0;

  return 1;
}

void stop()
{
  TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);

  while ((TWCR & (1 << TWSTO)))
    ;
}

uint8_t sendByte(uint8_t data)
{
  TWDR = data;

  TWCR = (1 << TWINT) | (1 << TWEN);

  while (!(TWCR & (1 << TWINT)))
    ;

  if (TWI_STATUS == 0x18 || TWI_STATUS == 0x28 || TWI_STATUS == 0x40)
    return 1;

  return 0;
}

uint8_t receiveByte(uint8_t ack, uint8_t *rx)
{
  if (ack)
    TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA);

  else
    TWCR = (1 << TWINT) | (1 << TWEN);

  while (!(TWCR & (1 << TWINT)))
    ;

  if (TWI_STATUS == 0x50 || TWI_STATUS == 0x58)
  {
    *rx = TWDR;
    return 1;
  }

  return 0;
}