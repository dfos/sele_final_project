#include <Arduino.h>
#include <inttypes.h>

#define I2C_CLK 100000
#define TWI_STATUS (TWSR & 0xF8)
#define START 0x08

void initialize();
uint8_t start();
uint8_t sendAddress(uint8_t);
uint8_t sendByte(uint8_t);
uint8_t receiveByte(uint8_t);
uint8_t receiveByte(uint8_t, uint8_t *target);
void stop();