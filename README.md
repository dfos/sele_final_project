## Introduction

This project was developed within the scope of the course of the electronic systems of the integrated master in electrical
and computer engineering at the University of Porto.
This course´s final project is composed by two different tasks:

*  Task 1 – Control remotely a ChipKIT Uno32 board just using its MCU JTAG port
*  Task 2 – Develop a system that uses an SPI or I2C device

## Task 1

### Resume

Knowing that the ChipKIT Uno32 board has a PIC32 microcontroller with JTAG capabilities it was proposed in the first phase of the project to develop a program to an Arduino board that receives UART commands from a PC and controls the PIC32 through its JTAG port using the corresponding IEEE 1149.1 TAP controller. The commands are composed by a single character and are the following:

*  d – Gets the ID CODE of the PIC32 and prints it in hexadecimal
*  1 – Turn on the LED LD5 of the ChipKIT board
*  0 – Turn off the LED LD5 of the ChipKIT board
*  b – Print the state of the button connected to pin 29 of the ChipKIT 

The JTAG interface (known also as a Test Access Port, or TAP), uses the following signals to support the operation of boundary scan:

*  TCK (Test Clock) – output signal that synchronizes the internal state machine operations.
*  TMS (Test Mode Select) – output signal that is sampled at the rising edge of TCK to determine the next state.
*  TDI (Test Data In) – input signal that represents the data shifted into the device’s test or programming logic. 
*  TDO (Test Data Out) – output signal that represents the data shifted out of the device’s test or programming logic.
*  TRST (Test Reset) – optional pin which, when available, can reset the TAP controller’s state machine.

The TAP controller, a state machine whose transitions are controlled by the TMS signal, is represented in the figure below. The TAP controller, as the name suggests, controls the behaviour of the JTAG system

 
![](images/tap_controller.png)

### Implementation

**Functions**
In this task, the following functions were implemented:

```c
void TMS(int value)
```
 Writes to TMS pin the logical 'value' specified

```c
void TCK()
```
Executes 1 clock cycle. That is, writes HIGH logical value to the clock pin, waits for 20 microsseconds, writes LOW logical value to the clock pin and waits for 20 microsseconds
```c
void TCK(int clocks)
```
Exactly the same as TCK() but loops for the specified amount of times
```c
void TDO(int value)
```
Writes to the TDO pin the logical 'value' specified
```c
void setTDO(unsigned char *value, char const *string)
```
Sets and shifts either IR or  DR, depending on the string argument, through TDO pin
```c
int readChar()
```
Reads character from the computer
```c
void resetTAP()
```
Sets the TAP controller to the Run-Test/Idle State
```c
void setSM(int value, int shifts)
```
Iterates over the state machine the number of 'shifts'. Final state is based on current state and the 'value' specified
```c
void led(boolean on)
```
Turn the LED either ON or OFF
```c
void getID()
```
Gets the IDCODE and prints it
```c
int buttonState()
```
Gets the state of the button
```c
void setup()
```
Sets the pins for output/input and starts the Serial communication with the computer
```c
void loop()
```
Asks the user for a character and executes accordingly
## Task 2

### Resume

In the second phase of this project, it was proposed the development of a small system using an SPI or I2C device. The TSL2561 luminosity sensor was chosen. It has a digital (i2c) interface. This task not only includes the development of the “shield” PCB (using EasyEDA) but also the firmware (using PlatformIO + VSCode) for the MCU that uses the device. The main goal is to avoid using Arduino libraries and instead use directly the registers present in the MCU for these synchronous serial communication.

### Firmware
**Functions**

```c
uint8_t power_on()
```
Turns on the sensor
```c
uint16_t getData(uint16_t *rx)
```
 Reads the 2 byte values from sensor's channel 0 (visible + infrared)
```c
void setup()
```
Starts Serial communication with computer and powers on sensor
```c
void loop()
```
Loops getData function and prints the read value
### Hardware

This is a relatively easy connection since it only requires the connection of the SDA and SCL pins between the component and the micro-controller and the VDD and GND connection as shown in Fig.\ref{fig:wiring}.


![Schematic](images/Schematic_final_SELE_T04_B04_20200110174934.png  "Schematic")

The shield PCB was developed using Easyeda. It is now possible to get the planned I2C system by inserting the component and the MCU into the PCB.

![PCB](images/Photo_View.svg "PCB")

### EasyEDA project

[Link to EasyEDA project](https://easyeda.com/joaosantos97/final)

## Report

[Link to project report](https://gitlab.com/dfos/sele_final_project/blob/master/SELE_T04_B04.pdf)

## Authors

* Diogo Santos up201505606
* João Pedro Santos up201504545
